# XML tools

## Licensing

![Creative Commons License](http://i.creativecommons.org/l/by/3.0/88x31.png)

This work is licensed under a
[Creative Commons Attribution 3.0 Unported License](https://creativecommons.org/licenses/by/3.0/).

This essentially means you can do anything you want as long as you retain the
attribution to the original author, Matthew Fernandez,
[matthew.fernandez@gmail.com](mailto:matthew.fernandez@gmail.com).

## XML patch

Like [`patch`](http://linux.die.net/man/1/patch), but takes an XML diff
conforming to the schema in schemas/patch.xsd and applies it to an XML document.
Usage:

`ruby xmlpatch.rb document.xml <patch.xml`

To spell out the format patches need to be in, they should have a single
top-level element `changes` that contains child elements named `add` or
`subtract`. These nodes each have a `path` attribute describing an
[xpath](http://www.w3.org/TR/xpath/) reference to a node in the target document
that is to be added or removed. When adding an element attribute you should
also specify `value` as an attribute of the relevant `add` element in the
patch. All subtractions are performed before additions.

There's more to know about the functionality of this tool, but it can mostly be
gleaned from the source.
