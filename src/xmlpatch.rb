#!/usr/bin/env ruby

# Disclaimer: I am not a Ruby programmer. If I'm doing something terrible here
# I would love for you to correct me so I won't make the same mistake again :)

require "nokogiri"

def patch(target_file)
    begin
        target = Nokogiri::XML(open(target_file, "r"))
    rescue Exception => e
        $stderr.puts "Failed to open patch target \"#{target_file}\": #{e.message}."
        return -1
    end

    patch = Nokogiri::XML(STDIN.read)

    # Some basic validation of the patch.
    if patch.xpath("/changes").children.length == 0
        $stderr.puts "Invalid patch supplied."
        return -1
    end

    # Sort the additions ascending as there may be some attributes or nodes
    # that are descendents of other additions that need to be made first. Note
    # that this logic of alphabetical sort falls over in the face of more
    # complicated xpath expressions.
    additions = patch.xpath("/changes").children.select { |x| x.name == "add" }
    additions.sort_by! {|x| x["path"]}

    # Sort the deletions descending as a patch may have redundant information
    # that removes attributes or nodes that are themselves descendents of nodes
    # that are to be removed.
    deletions = patch.xpath("/changes").children.select { |x| x.name == "subtract" }
    deletions.sort_by! {|x| x["path"]}.reverse

    # Do deletion steps first as any addition indicies are intended to apply to
    # the post-deletion state.
    deletions.each do |step|
        begin
            item = target.xpath(step["path"])
            if item.length == 0
                $stderr.puts "Failed: Cannot find path \"#{step["path"]}\" to delete."
                return -1
            end
            item.remove
        rescue Exception => e
            $stderr.puts "Failed: While removing \"#{step["path"]}\": #{e.message}."
            return -1
        end
    end

    additions.each do |step|
        begin
            path = step["path"]
            if target.xpath(path).length != 0
                $stderr.puts "Failed: Path \"#{path}\" to add already exists."
                return -1
            end
            parent_path, _, item = path.rpartition("/")
            parents = target.xpath(parent_path)
            if parents.length == 0
                $stderr.puts "Failed: Path \"#{path}\" to add has no existing parent."
                return -1
            end
            fail if parents.length != 1
            parent = parents[0]
            if item.start_with?("@")
                # This is an attribute.
                value = step["value"]
                if value.nil?
                    $stderr.puts "Failed: No value specified for path \"#{path}\" to add."
                    return -1
                end
                parent[item[1..-1]] = value
            else
                # This is a node. Use the last element of the xpath as the name
                # to create or allow the patch to override it with 'name'.
                child = Nokogiri::XML::Node.new (step["name"] or item), target

                # Allow the patch to set the 'id' attribute at the same time.
                # Necessary in some circumstances where you're adding multiple
                # nodes of the same name at the same level.
                child["id"] = step["id"]
                parent.add_child(child)
            end
        rescue Exception => e
            $stderr.puts "Failed: While adding \"#{step["path"]}\": #{e.message}."
            return -1
        end
    end

    # And we're done.
    open(target_file, "w") { |f| target.write_xml_to f }
    
    return 0
end

if __FILE__ == $0
    if ARGV.length != 1
        $stderr.puts "Usage: #{$0} file"
        exit(-1)
    end

    exit(patch(ARGV[0]))
end
