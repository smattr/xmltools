#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cp "${DIR}/original.svg" "${DIR}/patched.svg"
ruby "${DIR}/../../src/xmlpatch.rb" "${DIR}/patched.svg" <"${DIR}/patch.xml"
